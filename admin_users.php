<?php
include "connect.php";
require_once('models/People.php');
require_once('models/Users.php');
session_start();
?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <title>eduMath | Admin panel</title>
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body>

<header>
    <nav class="navbar fixed-top navbar-expand-xl navbar-light bg-white" style="font-size: 1.2rem">
        <div class="container">
            <a class="navbar-brand text-secondary" style="font-size: 2.2rem;" href="admin_users.php">eduMath | Admin Panel</a>
            <div class="btn-group mobile_lang">
                <a id="kz" class='btn btn-outline-secondary btn-sm translate'>Kz</a>
                <a id="ru" class='btn btn-outline-secondary btn-sm translate'>Ru</a>
                <a id="en" class='btn btn-outline-secondary btn-sm translate'>En</a>
            </div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">

                <ul class="navbar-nav nav- mb-2 mb-xl-0" style="margin-right: 10px;">
                    <li class="btn-group desktop_lang">
                        <a id="kz" class='btn btn-outline-secondary btn-sm translate'>Kz</a>
                        <a id="ru" class='btn btn-outline-secondary btn-sm translate'>Ru</a>
                        <a id="en" class='btn btn-outline-secondary btn-sm translate'>En</a>
                    </li>
                </ul>

                <ul class="navbar-nav mb-2 mb-xl-0">
                    <li class='nav-item'><a class='btn btn-primary' href='index.php'>Back to website</a></li>
                </ul>
            </div>
        </div>
    </nav>
</header>

<div class="container" style="margin-top: 100px;">
    <div class="row justify-content-center" style="text-align: center;">
        <div class="col-8 mb-2">
            <h3>User management</h3>
            <hr class="badge-secondary mt-0 w-70 mx-auto">
            <?php
            if(isset($_SESSION['status']))
            {
                ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Hey!</strong> <?php echo $_SESSION['status']; ?>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
                <?php
                unset($_SESSION['status']);
            }
            ?>
        </div>
    </div>
</div>
<div class="container">
    <div class="row ">
        <div class="col-md-2 nav-pills">
            <a href="#" class="nav-link bg-secondary active">Users</a>
            <a href="admin_courses.php" class="nav-link">Courses</a>
<!--            <a href="admin_comments.php" class="nav-link">Comments</a>-->
        </div>
        <div class="col-md">
            <table class="table">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">First name</th>
                    <th scope="col">Last name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Delete</th>
                </tr>
                </thead>
                <tbody>
                <?php

                $stmt = $link->prepare("SELECT * FROM users WHERE type !=777");
                $stmt->execute();
                $res = $stmt->get_result();

                $stmt->close();

                if (mysqli_num_rows($res) > 0) {
                    while ($row = $res->fetch_assoc()) {
                        echo

                            '<tr>
                                <th scope="row">' . $row["user_id"] . '</th>
                                <td>' . $row["name"] . '</td>
                                <td>' . $row["surname"] . '</td>
                                <td>' . $row["email"] . '</td>
                                <td>                 
                                              
                                <!-- Button trigger modal -->                                 
                                </td>
                                <td><a href="delete_user.php?id='. $row["user_id"] . '" class="btn btn-danger">Delete</a></td>
                            </tr>';
                    }
                }
                ?>
                </tbody>
            </table>

        </div>
    </div>
</div>
</div>


<!-- Modal -->
<form method="POST" action="update_password.php">

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Change password</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="form-group d-flex align-items-center justify-content-center">
                    <div class="col-3">
                        <label>New password:</label>
                    </div>
                    <div class="col">
                        <input type="password" class="form-control" id="newPass" name="newpass1" placeholder=" ">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-success" name="save_pass">Save</button>
            </div>
        </div>
    </div>
</div>
</form>
<script type="text/javascript">
    var myModal = document.getElementById('exampleModal')
    var myInput = document.getElementById('btn_m')

    myModal.addEventListener('shown.bs.modal', function () {
        myInput.focus()
    })
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="js/index.js"></script>

</body>
</html>




