<?php
include "connect.php";
require_once('models/People.php');
require_once('models/Users.php');
session_start();

//if user not logged in, this page will not be showed
if(!isset($_SESSION['id'])) header("Location: login.php");

$stmt = $link->prepare("SELECT * FROM users WHERE user_id = ? LIMIT 1");
$stmt->bind_param("i", $_SESSION['id']);

$stmt->execute();

$result = $stmt->get_result();

$row = $result->fetch_assoc();
if ($row == null) return null;
$user = new Users($row['email'], $row['name'], $row['surname'], $row['phone'], $row['password'], $row['social_link'], $row['about'], $row['age'], $row['city']);

if ($user == null) {
    header("Location: index.php?logout");
    return;
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <title>eduMath | <?php echo $user->getName(); ?> <?php echo $user->getSurname(); ?></title>
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body>

<header>
    <nav class="navbar fixed-top navbar-expand-xl navbar-light bg-white" style="font-size: 1.2rem">
        <div class="container">
            <a class="navbar-brand text-secondary" style="font-size: 2.2rem;" href="index.php">eduMath</a>
            <div class="btn-group mobile_lang">
                <a id="kz" class='btn btn-outline-secondary btn-sm translate'>Kz</a>
                <a id="ru" class='btn btn-outline-secondary btn-sm translate'>Ru</a>
                <a id="en" class='btn btn-outline-secondary btn-sm translate'>En</a>
            </div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-xl-0">
                    <li class="nav-item">
                        <a class="nav-link lang" aria-current="page" href="index.php#home" key="home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link lang" href="index.php#about" key="about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link lang" href="index.php#contacts" key="contacts">Contacts</a>
                    </li>
                </ul>

                <ul class="navbar-nav nav- mb-2 mb-xl-0" style="margin-right: 10px;">
                    <li class="btn-group desktop_lang">
                        <a id="kz" class='btn btn-outline-secondary btn-sm translate'>Kz</a>
                        <a id="ru" class='btn btn-outline-secondary btn-sm translate'>Ru</a>
                        <a id="en" class='btn btn-outline-secondary btn-sm translate'>En</a>
                    </li>
                </ul>

                <ul class="navbar-nav mb-2 mb-xl-0">
                    <?php
                    if (!isset($_SESSION['id'])) {
                        echo " <li class='nav-item'><a class='btn btn-secondary lang' href='login.php' key='login'>Login</a></li>";
                    }
                    else{
                        if ($_SESSION['type'] == 2) {
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='constructor.php' key='constructor'>Constructor</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                        else{
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
</header>

<div class="container" style="margin-top: 100px;">
    <div class="row justify-content-center" style="text-align: center;">
        <div class="col-8 mb-2">
            <h3>Profile setting</h3>
            <hr class="badge-secondary mt-0 w-70 mx-auto">
        </div>
    </div>
</div>
<div class="container">
        <div class="row ">
            <div class="col-md-2 nav-pills">
                <a href="#" class="nav-link bg-secondary active">Edit profile</a>
<!--                <a href="change_email.php" class="nav-link">Change email</a>-->
                <a href="change_password.php" class="nav-link">Change password</a>
                <a href="change_add_info.php" class="nav-link">Additional information</a>
            </div>
            <div class="col-md-8">
                <?php
                if(isset($_SESSION['status']))
                {
                    ?>
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>Hey!</strong> <?php echo $_SESSION['status']; ?>
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    <?php
                    unset($_SESSION['status']);
                }
                ?>
                <form method="POST" action="update_profile.php">
                    <div class="form-group d-flex align-items-center justify-content-center">
                        <div class="col-3">
                            <label>Name:</label>
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" id="name" name="name" value="<?php echo $user->getName(); ?>" required>
                        </div>
                    </div>
                    <div class="form-group d-flex align-items-center justify-content-center">
                        <div class="col-3">
                            <label>Surname:</label>
                        </div>
                        <div class="col">
                            <input type="text" class="form-control" id="surname" name="surname" value="<?php echo $user->getSurname(); ?>" required>
                        </div>
                    </div>
                    <div class="form-group d-flex align-items-center justify-content-center">
                        <div class="col-3">
                            <label>Phone:</label>
                        </div>
                        <div class="col">
                            <input type="tel" class="form-control" id="phone" name="phone" value="<?php echo $user->getPhone(); ?>" pattern="[0-9]{1}[0-9]{3}[0-9]{3}[0-9]{2}[0-9]{2}" required>
                            <div id="phoneExample" class="form-text">
                                Example: 87771234567
                            </div>
                        </div>
                    </div>

                    <div class="form-group d-flex align-items-center justify-content-center">
                        <div class="col-3">
                        </div>
                        <div class="col">
                            <button type="submit" class="btn btn-success" name="update">Update</button>
                            <a href="myprofile.php" class="btn btn-danger">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="js/index.js"></script>
</body>
</html>



