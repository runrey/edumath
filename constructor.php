<?php
include "connect.php";
require_once('models/People.php');
require_once('models/Users.php');
session_start();

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['name']);
    unset($_SESSION['id']);
    header("location: index.php");
}

if (isset($_GET['create'])) {
    if ($_GET['create'] != NULL) {
        $stmt = $link->prepare("INSERT INTO courses(name, author, status) VALUES (?,?, 'draft')");
        $stmt->bind_param("si", $_GET['create'], $_SESSION['id']);
        $results = $stmt->execute();
        $stmt->close();
        $_SESSION['type'] = 2;
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <title>eduMath | Constructor</title>
    <link rel="stylesheet" href="css/course.css">
</head>
<body>

<!-- Navbar -->
<header>
    <div class="container-sm" >
        <nav class="navbar fixed-top navbar-expand-xl navbar-light bg-white" style="font-size: 1.2rem">
            <div class="container">
                <a class="navbar-brand text-secondary" style="font-size: 2.2rem;" href="index.php">eduMath</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav me-auto mb-2 mb-xl-0">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="index.php#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php#contacts">Contacts</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav mb-2 mb-xl-0">
                        <?php
                        if (!isset($_SESSION['id'])) {
                            echo "<li class='nav-item' ><a class='btn btn-secondary' href='login.php'>Login</a></li>";
                        }
                        else{
                            if ($_SESSION['type'] == 2) {
                                echo
                                "<li class='nav-item'><a class='nav-link lang' href='constructor.php' key='constructor'>Constructor</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                            }
                            else{
                                echo
                                "<li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>

<div style="margin-top: 150px; display: flex; width: 100vw; align-items: center; justify-content: center; flex-direction: column">
    <h1>Your courses</h1>
    <br>
    <form class="d-flex" method="get" style="width: 300px;">
        <input class="form-control me-2" type="search" id="search" name="create" placeholder="Type course name..." aria-label="Search">
        <button class="btn btn-outline-success me-2" type="submit" class="submit">Create</button>
    </form>
</div>

<!-- Courses section -->
<div class="courses">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <?php
                $stmt = $link->prepare("SELECT * FROM courses where status='draft' and author='".$_SESSION['id']."'");
                $stmt->execute();
                $res = $stmt->get_result();

                $stmt->close();

                if (mysqli_num_rows($res) > 0) {
                    while ($row = $res->fetch_assoc()) {
                        echo '
                    <div class="col-lg-2 col-sm-6 mb-4" >
                      <div class="card">
                        <img src="images/course_example_image.png" class="card-img-top" alt="card-grid-image">
                        <div class="card-body">
                          <h5 class="card-title" style="overflow:hidden;height: 50px;">' . $row["name"] . '</h5>
                          <p class="card-text" style="overflow:hidden;height: 70px;"><small class="text-muted">' . $row["description"] . '</small></p>
                          <a href="course_constructor.php?id=' . $row['course_id'] . '" class="btn btn-outline-dark">Edit</a>
                        </div>
                      </div>
                    </div>
                    ';
                    }
                }


                if (mysqli_num_rows($res) == 0) {
                    echo '<p>NO courses!</p>';
                }

            ?>
        </div>

    </div>
</div>

<!-- Including custom js file and bootstrap js files -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script src="https://use.fontawesome.com/99e1cb3fcf.js"></script>
</body>
</html>