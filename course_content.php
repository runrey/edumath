<?php
session_start();

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['name']);
    unset($_SESSION['id']);
    header("location: index.php");
}
include "connect.php";

if (isset($_GET['id'])) {
    $stmt = $link->prepare("SELECT * FROM courses WHERE course_id = ?");
    $stmt->bind_param("i", $_GET['id']);
    $stmt->execute();
    $res = $stmt->get_result();

    $stmt->close();

    if (mysqli_num_rows($res) > 0) {
        $row = $res->fetch_assoc();
    }
}
if (isset($_GET['check'])) {
    $stmt = $link->prepare("UPDATE user_progress SET achieved=1 WHERE user_id = ? and lesson_line_id = ?");
    $stmt->bind_param("ii", $_SESSION['id'], $_GET['check']);
    $stmt->execute();
    $res = $stmt->get_result();

    $stmt->close();
}
?>
<!DOCTYPE html>
<html lang="kz">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <title>eduMath | Lessons</title>
    <link rel="stylesheet" href="css/course.css">
</head>
<body>

<!-- Navbar -->
<header>
    <div class="container-sm" >
        <nav class="navbar fixed-top navbar-expand-xl navbar-light bg-white" style="font-size: 1.2rem">
            <div class="container">
                <a class="navbar-brand text-secondary" style="font-size: 2.2rem;" href="index.php">eduMath</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav me-auto mb-2 mb-xl-0">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="index.php#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php#contacts">Contacts</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav mb-2 mb-xl-0">
                        <?php
                        if (!isset($_SESSION['id'])) {
                            echo "<li class='nav-item' ><a class='btn btn-secondary' href='login.php'>Login</a></li>";
                        }
                        else{
                            if ($_SESSION['type'] == 2) {
                                echo
                                "<li class='nav-item'><a class='nav-link lang' href='constructor.php' key='constructor'>Constructor</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                            }
                            else{
                                echo
                                "<li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>


<div class="container mt-5 __course">
    <div class="row justify-content-center" style="text-align: center">
        <div class="col-4 mb-2">
            <h1><?php echo $row['name']; ?></h1>
        </div>
    </div>
</div>

<?php
$stmt = $link->prepare("SELECT COUNT(*) FROM user_progress WHERE user_id = ? and course_id=?");
$stmt->bind_param("ii", $_SESSION['id'],$_GET['id']);
$stmt->execute();
$res7 = $stmt->get_result();
$result7 = $res7->fetch_array();


$stmt = $link->prepare("SELECT COUNT(*) FROM user_progress WHERE user_id = ? and course_id=? and achieved=1");
$stmt->bind_param("ii", $_SESSION['id'],$_GET['id']);
$stmt->execute();
$res8 = $stmt->get_result();
$stmt->close();
$result8 = $res8->fetch_array();
if($result8[0] != 0){
    $progress = $result7[0]/$result8[0]*100;
}
else{
    $progress = 0;
}

?>
<div class="container mt-5">
    <div class="row justify-content-center" style="text-align: center">
        <div class="col-4 mb-2">
            <h4>Progress: <?php echo $progress; ?>%</h4>
        </div>
    </div>
</div>

<!-- Courses section -->
<div class="courses">
    <div class="container">
        <div class="row justify-content-center mt-5">

            <div class="accordion" id="accordionExample">

                <?php
                $stmt = $link->prepare("SELECT * FROM lessons WHERE course_id = ?");
                $stmt->bind_param("i", $_GET['id']);
                $stmt->execute();
                $res1 = $stmt->get_result();

                $stmt->close();

                if (mysqli_num_rows($res1) > 0) {
                    while ($lesson = $res1->fetch_assoc()) {
                        echo '
                <div class="accordion-item">
                    <h2 class="accordion-header" id="headingOne">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            '.$lesson["name"].'
                        </button>
                    </h2>';
                        $stmt = $link->prepare("SELECT * FROM lesson_lines WHERE lesson_id = ?");
                        $stmt->bind_param("i", $lesson['lesson_id']);
                        $stmt->execute();
                        $res2 = $stmt->get_result();
                        $stmt->close();

                        if (mysqli_num_rows($res2) > 0) {
                            while ($lines = $res2->fetch_assoc()) {

                                $stmt = $link->prepare("SELECT * FROM user_progress WHERE user_id = ? and lesson_line_id=?");
                                $stmt->bind_param("ii", $_SESSION['id'],$lines['lesson_line_id']);
                                $stmt->execute();
                                $res6 = $stmt->get_result();

                                $stmt->close();
                                if (mysqli_num_rows($res6) > 0) {
                                    $progress = $res6->fetch_assoc();
                                }

                                echo '
                    <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                        <div class="accordion-body">
                            <h3>'.$lines['kz_heading'].'</h3>
                            '.$lines['kz_lecture_text'].'
                            <br><br>
                            <h3>Video material</h3>
                            <iframe width="420" height="315"
                            src="'.$lines['kz_video_url'].'">
                            </iframe>
                            
                            <br><br>
                            <h3>Self-study material</h3>
                            <a class="text-dark" style="font-size: 1.5rem; text-decoration: underline" href="'.$lines['kz_file_url'].'">Click to download</a>
                        </div>
                        <br><br>';
                                if($progress['achieved'] == 0){
                                    echo '<a href="course_content.php?id='.$_GET["id"].'&check='.$lines["lesson_line_id"].'" class="btn btn-success">Check the lesson</a>';
                                }

                                echo '
                        <br><br><br><br><br><br>
                    </div>';
                            }
                            echo '</div>';
                        }
                    }

                }
                else{
                    echo '<h2>No lessons added yet. Sorry :(</h2>';
                }


                ?>
            </div>

        </div>
    </div>
</div>

<!-- Footer -->



<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="/js/index.js"></script>
</body>
</html>