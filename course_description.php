<?php
session_start();
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['name']);
    unset($_SESSION['id']);
    header("location: index.php");
}
include "connect.php";
if (isset($_GET['id'])) {
    $stmt = $link->prepare("SELECT * FROM courses WHERE course_id = ?");
    $stmt->bind_param("i", $_GET['id']);
    $stmt->execute();
    $res = $stmt->get_result();

    $stmt->close();

    if (mysqli_num_rows($res) > 0) {
        $row = $res->fetch_assoc();
    }

    if (isset($_SESSION['id'])){
        $stmt1 = $link->prepare("SELECT * FROM users_courses_rel WHERE courses_id = ? and user_id = ?");
        $stmt1->bind_param("ii", $_GET['id'], $_SESSION['id']);
        $stmt1->execute();
        $res1 = $stmt1->get_result();

        $stmt1->close();
    }


}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.1/css/all.min.css"/>
    <title>eduMath | Course</title>
    <link rel="stylesheet" href="css/course_description.css">
</head>
<body>
<!-- Navbar -->
<div class="container-sm" >
    <nav class="navbar fixed-top navbar-expand-xl navbar-light bg-white" style="font-size: 1.2rem">
        <div class="container">
            <a class="navbar-brand text-secondary" style="font-size: 2.2rem;" href="index.php">eduMath</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-xl-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="index.php#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php#contacts">Contacts</a>
                    </li>
                </ul>
                <ul class="navbar-nav mb-2 mb-xl-0">
                    <?php
                    if (!isset($_SESSION['id'])) {
                        echo "<li class='nav-item' ><a class='btn btn-secondary' href='login.php'>Login</a></li>";
                    }
                    else{
                        if ($_SESSION['type'] == 2) {
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='constructor.php' key='constructor'>Constructor</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                        else{
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
</div>
<!-- Navbar Ends -->



<!--User Information -->
<div class="container mt-5">
    <div class="row d-flex justify-content-center" >
        <div class="col-md-10 mt-5 pt-5">
            <?php
            if (mysqli_num_rows($res) > 0) {?>
                <div class="row shadow">
                    <div class="col-sm-4 bg-warning rounded-left">
                        <div class="card-block text-center text-white">
                            <i class="bi bi-book mt-5" style="font-size: 8rem;"></i>
                            <h2 class="font-weight-bold"><?php echo $row['name']; ?></h2>
                            <p class="text-white"><?php echo $row['author']; ?></p>
                            <?php
                            if (!isset($_SESSION['id'])) {
                                echo '<a href="login.php" class="btn btn-outline-dark mt-2 mb-4 btn-lg">Add course</a>';
                            }
                            else{
                                if (mysqli_num_rows($res1) > 0) {
                                    echo '<a href="myprofile.php?alert='.$_GET['id'].'" class="btn btn-outline-dark mt-2 mb-4 btn-lg">Add course</a>';
                                }
                                else{
                                    echo '<a href="myprofile.php?add='.$_GET['id'].'" class="btn btn-outline-dark mt-2 mb-4 btn-lg">Add course</a>';
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="col-sm-8 bg-light rounded-right">
                        <h3 class="mt-3  mb-4 text-center">Description</h3>
                        <div class="row">
                            <div class="col-sm-12">
                                <h6 class="text-muted"><?php echo $row['description']; ?></h6>
                            </div>
                        </div>
                        <h5 class="mt-3 text-secondary">Details</h5>
                        <hr class="badge-secondary border-3">
                        <div class="row">
                            <div class="col-sm-6">
                                <p class="font-weight-bold text-secondary">Lessons: </p>
                                <h6 class="text-muted"><?php echo $row['lesson_count']; ?></h6>
                            </div>
                            <div class="col-sm-6">
                                <p class="font-weight-bold text-secondary">Time: </p>
                                <h6 class="text-muted"><?php echo $row['time']; ?></h6>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } else{?>
                <h1>No such course found!</h1>
            <?php }?>
        </div>
    </div>
</div>




<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="/js/index.js"></script>
</body>
</html>