<?php
session_start();
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['name']);
    unset($_SESSION['id']);
    header("location: index.php");
}
include "connect.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <title>eduMath | Courses</title>
    <link rel="stylesheet" href="css/course.css">
</head>
<body>

<!-- Navbar -->
<div class="container-sm" >
    <nav class="navbar fixed-top navbar-expand-xl navbar-light bg-white" style="font-size: 1.2rem">
        <div class="container">
            <a class="navbar-brand text-secondary" style="font-size: 2.2rem;" href="index.php">eduMath</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-xl-0">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="index.php#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php#about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="index.php#contacts">Contacts</a>
                    </li>
                </ul>
                <ul class="navbar-nav mb-2 mb-xl-0">
                    <?php
                    if (!isset($_SESSION['id'])) {
                        echo "<li class='nav-item' ><a class='btn btn-secondary' href='login.php'>Login</a></li>";
                    }
                    else{
                        if ($_SESSION['type'] == 2) {
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='constructor.php' key='constructor'>Constructor</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                        else{
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
</div>

<!-- Search box -->
<div class="container" style="margin-top: 100px;">
    <div class="row justify-content-center" style="text-align: center;">
        <div class="col-4 mb-2">
            <h1>Online courses</h1>
            <hr class="badge-secondary mt-0 w-50 mx-auto">
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-10">
            <form class="d-flex" method="get">
                <input class="form-control me-2" type="search" id="search" name="search" placeholder="Type course name..." aria-label="Search">
                <button class="btn btn-outline-success me-2" type="submit" class="submit">Search</button>
            </form>
        </div>
    </div>
</div>

<!-- Courses section -->
<div class="courses">
    <div class="container">
        <div class="row justify-content-center mt-5">
            <?php
            if (!isset($_GET['search'])) {
                $stmt = $link->prepare("SELECT * FROM courses where status !='draft'");
                $stmt->execute();
                $res = $stmt->get_result();

                $stmt->close();

                if (mysqli_num_rows($res) > 0) {
                    while ($row = $res->fetch_assoc()) {
                        echo '
                    <div class="col-lg-2 col-sm-6 mb-4" >
                      <div class="card">
                        <img src="images/course_example_image.png" class="card-img-top" alt="card-grid-image">
                        <div class="card-body">
                          <h5 class="card-title" style="overflow:hidden;height: 50px;">' . $row["name"] . '</h5>
                          <p class="card-text" style="overflow:hidden;height: 70px;"><small class="text-muted">' . $row["description"] . '</small></p>
                          <p class="card-price" style="color: black;">' . $row["price"] . ' tg</p>
                          <a href="course_description.php?id=' . $row['course_id'] . '" class="btn btn-outline-dark">Learn more</a>
                        </div>
                      </div>
                    </div>
                    ';
                    }
                }


                if (mysqli_num_rows($res) == 0) {
                    echo '<p>NO courses!</p>';
                }
            }
            else{
                $stmt = $link->prepare("SELECT * FROM courses WHERE name like ? and status !='draft'");
                $search = '%'.$_GET['search'].'%';
                $stmt->bind_param("s", $search);
                $stmt->execute();
                $res = $stmt->get_result();

                $stmt->close();

                if (mysqli_num_rows($res) > 0) {
                    while ($row = $res->fetch_assoc()) {
                        echo '
                    <div class="col-lg-2 col-sm-6 mb-4" >
                      <div class="card">
                        <img src="images/course_example_image.png" class="card-img-top" alt="card-grid-image">
                        <div class="card-body">
                          <h5 class="card-title" style="overflow:hidden;height: 50px;">' . $row["name"] . '</h5>
                          <p class="card-text" style="overflow:hidden;height: 70px;"><small class="text-muted">' . $row["description"] . '</small></p>
                          <p class="card-price" style="color: black;">' . $row["price"] . ' tg</p>
                          <a href="course_description.php?id=' . $row['course_id'] . '" class="btn btn-outline-dark mt-">Go to course</a>
                        </div>
                      </div>
                    </div>
                    ';
                    }
                }


                if (mysqli_num_rows($res) == 0) {
                    echo '<p>NO courses!</p>';
                }
            }

            ?>
        </div>

    </div>
</div>

<!-- Best authors -->
<section>
    <div class="container" id="container_team">
        <div class="header">
            <h1>Project team</h1>
        </div>
        <div class="sub-container">
            <div class="teams">
                <img src="images/profile_picture.png" alt="">
                <div class="name">Aldabergenov Alikhan</div>
                <div class="desig">Developer</div>
                <div class="about"> </div>

                <div class="social-links">
                    <a href="https://google.com"><i class="bi bi-google"></i></a>
                    <a href="https://instagram.com"><i class="bi bi-instagram"></i></a>
                    <a href="https://whatsapp.com"><i class="bi bi-whatsapp"></i></a>
                    <a href="https://github.com"><i class="bi bi-github"></i></a>
                </div>
            </div>

            <div class="teams">
                <img src="images/profile_picture.png" alt="">
                <div class="name">Bolatkanov Yernur</div>
                <div class="desig">Developer</div>
                <div class="about"> </div>

                <div class="social-links">
                    <a href="https://google.com"><i class="bi bi-google"></i></a>
                    <a href="https://instagram.com"><i class="bi bi-instagram"></i></a>
                    <a href="https://whatsapp.com"><i class="bi bi-whatsapp"></i></a>
                    <a href="https://github.com"><i class="bi bi-github"></i></a>
                </div>
            </div>

            <div class="teams">
                <img src="images/profile_picture.png" alt="">
                <div class="name">Kalpakov Yerbolat</div>
                <div class="desig">Project Manager</div>
                <div class="about"> </div>

                <div class="social-links">
                    <a href="https://google.com"><i class="bi bi-google"></i></a>
                    <a href="https://instagram.com"><i class="bi bi-instagram"></i></a>
                    <a href="https://whatsapp.com"><i class="bi bi-whatsapp"></i></a>
                    <a href="https://github.com"><i class="bi bi-github"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- Footer -->
<footer class="w-100 py-4 flex-shrink-0">
    <div class="container py-4">
        <div class="row gy-4 gx-5">
            <div class="col-lg-4 col-md-6">
                <h5 class="h1 text-white">eduMath</h5>
                <p class="small text mb-0">&copy; Copyrights. All rights reserved. <a class="text-primary" href="#">edumath.com</a></p>
            </div>
            <div class="col-lg-2 col-md-6">
                <h5 class="text-white mb-3">Quick links</h5>
                <ul class="list-unstyled text">
                    <li><a href="index.php#home">Home</a></li>
                    <li><a href="index.php#about">About</a></li>
                    <li><a href="index.php#contacts">Contacts</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6">
                <h5 class="text-white mb-3">Newsletter</h5>
                <p class="small text">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                <form action="#">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="button-addon2">
                        <button class="btn btn-primary" id="button-addon2" type="button"><i class="bi bi-send-fill"></i></button>
                    </div>
            </div>
        </div>
</footer>
<!-- Footer ends -->


<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="/js/index.js"></script>
<script src="/js/sort.js"></script>
</body>
</html>