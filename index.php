<?php
session_start();
if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['name']);
    unset($_SESSION['id']);
    header("location: index.php");
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <title>eduMath</title>
    <link rel="stylesheet" href="css/index.css">
    <script type="text/javascript" src="js/jquery.min.js"></script>
</head>
<body>

<header>
    <nav class="navbar fixed-top navbar-expand-xl navbar-light bg-white" style="font-size: 1.2rem">
        <div class="container">
            <a class="navbar-brand text-secondary" style="font-size: 2.2rem;" href="index.php">eduMath</a>
            <div class="btn-group mobile_lang">
                <a id="kz" class='btn btn-outline-secondary btn-sm translate'>Kz</a>
                <a id="ru" class='btn btn-outline-secondary btn-sm translate'>Ru</a>
                <a id="en" class='btn btn-outline-secondary btn-sm translate'>En</a>
            </div>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav me-auto mb-2 mb-xl-0">
                    <li class="nav-item">
                        <a class="nav-link lang" aria-current="page" href="index.php#home" key="home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link lang" href="index.php#about" key="about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link lang" href="index.php#contacts" key="contacts">Contacts</a>
                    </li>
                </ul>

                <ul class="navbar-nav nav- mb-2 mb-xl-0" style="margin-right: 10px;">
                    <li class="btn-group desktop_lang">
                        <a id="kz" class='btn btn-outline-secondary btn-sm translate'>Kz</a>
                        <a id="ru" class='btn btn-outline-secondary btn-sm translate'>Ru</a>
                        <a id="en" class='btn btn-outline-secondary btn-sm translate'>En</a>
                    </li>
                </ul>

                <ul class="navbar-nav mb-2 mb-xl-0">
                    <?php
                    if (!isset($_SESSION['id'])) {
                        echo " <li class='nav-item'><a class='btn btn-secondary lang' href='login.php' key='login'>Login</a></li>";
                    }
                    else{
                        if ($_SESSION['type'] == 2) {
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='constructor.php' key='constructor'>Constructor</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                        else if ($_SESSION['type'] == 777) {
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='admin_users.php' key='apanel'>Admin Panel</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                        else{
                            echo
                            "<li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>
</header>

<section class="main">
    <div class="container">
        <div class="row promo_1 justify-content-center align-items-center" id="home">
            <div class="col-lg-4">
                <div class="block text-center text-lg-start">
                    <h1  class="lang" style="font-size: 3rem;"  key="header1">Amazing<br> courses for your <br>education</h1>
                    <p class="text-muted lang" style="font-size: 1.5rem;" key="paragraph1">Make your math greater</p>
                    <a href='courses.php' class='btn btn-warning btn-lg lang' key="btn_get_started">Get Started</a>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="block">
                    <img src="images/bg_image.png" class="img-fluid" alt="img-fluid">
                </div>
            </div>
        </div>
    </div>
    <div class="container-bg bg-light">
        <div class="container">
            <div class="row promo_2 justify-content-center align-items-center bg-light" id="about">
                <div class="col-lg-8">
                    <div class="block">
                        <img src="images/bg_image_2.png" class="img-fluid" alt="img-fluid">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="block text-center text-lg-start">
                        <h1 class="lang" style="font-size: 3rem;" key="header2">Online <br> courses for math</h1>
                        <p class="text-muted lang" style="font-size: 1.5rem;"  key="paragraph2">Make your math greater</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-bg bg-light">
        <div class="container">
            <div class="row promo_2_2 justify-content-center align-items-center" id="about">
                <div class="col-lg-4">
                    <div class="block text-center text-lg-start">
                        <h1 class="lang" style="font-size: 3rem;" key="header2">Online <br> courses for math</h1>
                        <p class="text-muted lang" style="font-size: 1.5rem;"  key="paragraph2">Make your math greater</p>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="block">
                        <img src="images/bg_image_2.png" class="img-fluid" alt="img-fluid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row promo_3 justify-content-center align-items-center" id="contacts">
            <div class="col-lg-4 text-center text-lg-start">
                <div class="block">
                    <h1 class="lang"  style="font-size: 3rem;" key="header3">Ask your questions</h1>
                    <p class="text-muted" style="font-size: 1.5rem;">edumath@gmail.com</p>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="block">
                    <img src="images/bg_image_3.png" class="img-fluid" alt="img-fluid">
                </div>
            </div>
        </div>
    </div>

</section>

<!-- Footer -->
<footer class="w-100 py-4 flex-shrink-0">
    <div class="container py-4">
        <div class="row gy-4 gx-5 justify-content-between ">
            <div class="col-lg-4 col-md-6">
                <h5 class="h1 text-white">eduMath</h5>
                <p class="small text mb-0 lang" key="laws">&copy; Copyrights. All rights reserved. <a class="text-primary" href="#">edumath.com</a></p>
            </div>
            <div class="col-lg-2 col-md-6">
                <h5 class="text-white mb-3 lang" key="quick_links">Quick links</h5>
                <ul class="list-unstyled text">
                    <li><a href="#" class="lang" key="home">Home</a></li>
                    <li><a href="#" class="lang" key="about">About</a></li>
                    <li><a href="#" class="lang" key="contacts">Contacts</a></li>
                </ul>
            </div>
            <div class="col-lg-4 col-md-6">
                <h5 class="text-white mb-3 lang">Newsletter</h5>
                <p class="small text lang">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt.</p>
                <form action="#">
                    <div class="input-group mb-3">
                        <input class="form-control" type="text" placeholder="Username" aria-label="Username" aria-describedby="button-addon2">
                        <button class="btn btn-primary" id="button-addon2" type="button"><i class="bi bi-send-fill"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</footer>
<!-- Footer ends -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="js/index.js"></script>
</body>
</html>