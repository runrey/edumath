<?php
include "connect.php";

session_start();
$error = array();

if(!empty($_POST['email']) && !empty($_POST['password'])){

    $email = $_POST['email'];
    $password = md5($_POST['password']);

    $stmt = $link->prepare("SELECT * FROM users WHERE email = ? and password = ?");
    $stmt->bind_param("ss", $email, $password);
    $stmt->execute();
    $res = $stmt->get_result();

    $stmt->close();

    if(mysqli_num_rows($res)>0){
        $row = $res->fetch_assoc();
        $_SESSION['id'] = $row['user_id'];
        $_SESSION['name'] = $row['name'];
        $_SESSION['type'] = $row['type'];
        array_push($error, "good");
    }

    if(mysqli_num_rows($res)==0){
        array_push($error, "Wrong password");
    }
}

else{
    array_push($error, "Not all fields are filled!");
}

echo json_encode($error);
?>