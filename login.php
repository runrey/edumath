<?php
session_start();

if (isset($_SESSION['id'])) {
    # code...
    header('Location: index.php');
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eduMath | Login</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
<div class="login_main">
    <form method="POST" action="login.php">
        <div class="login_form_body" data-aos="zoom-in" data-aos-duration="300">
            <h2>Welcome back!</h2>
            <div class="login_form_elems">
                <div class="reg-status">
                    <p id="reg-status"></p>
                </div>
                <div class="login_form_elem">
                    <label for="email">E-MAIL</label>
                    <input type="email" id="email" name="email" required>
                </div>
                <div class="login_form_elem">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" required>
                    <a href="#">Forgot your password?</a>
                </div>

                <div class="login_form_elem">
                    <button class="btn btn-warning submit" type="submit" name="submit">Submit</button>
                    <p>Don't have an account yet? <a href="signup.php">Sign up here</a></p>
                </div>
            </div>
        </div>
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script>
    $(document).ready(function() {
        $(".submit").click(function(){
            event.preventDefault();
            var email = $('#email').val();
            var password = $('#password').val();
            $.post('log.php', { email : email, password : password },
                function(data, status){
                    $('.reg-status').fadeIn(300);

                    var dat = JSON.parse(data);
                    if(dat[0]!=="good"){
                        var str = "";
                        for(var i = 0; i<dat.length; i++){
                            str += dat[i] + "<br>";
                        }
                        $('#reg-status').html(str).css('color', 'red');
                    }
                    else{
                        window.location.href = "index.php";
                    }
                });
        });
    });

    $("#email").bind('input', function () {
        var email = $('#email').val();
        if(email !== '') {
            $.ajax({
                type: "POST",
                url: "checker.php",
                data: { username : username },
                accepts: 'application/json; charset=utf-8',
                success: function (data) {
                    data = JSON.parse(data);
                    for (var key in data)
                    {
                        if(data.message === 'available'){
                            $('.reg-status').fadeIn(300);
                            $('#reg-status').text("No such e-mail found!").css('color', 'red');
                        }
                        if(data.message === 'not-available'){
                            $('.reg-status').fadeIn(500);
                            $('#reg-status').text("This e-mail is used already!").css('color', 'green');
                        }
                    }
                },
                error: function (httpRequest, status, error) {
                    $('#reg-status').text('Error: ' + error + ', ' + httpRequest.status).css('color', 'red');
                }
            });
        }
        if(email.length === 0){
            $('.reg-status').fadeOut(300);
        }
    });
</script>
<script src="js/index.js"></script>
</body>
</html>
