<?php


abstract class People
{
    //variables
    private $email;
    private $name;
    private $surname;
    private $phone;
    private $password;
    private $social;
    private $about;
    private $age;
    private $city;

    //constructor
    public function __construct($email, $name, $surname, $phone, $password,$social,$about,$age,$city)
    {
        $this->setEmail($email);
        $this->setName($name);
        $this->setSurname($surname);
        $this->setPhone($phone);
        $this->setPassword($password);
        $this->setSocial($social);
        $this->setAbout($about);
        $this->setAge($age);
        $this->setCity($city);
    }

    //setter and getter methods
    public function getEmail()
    {
        return $this->email;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSocial()
    {
        return $this->social;
    }

    public function getAbout()
    {
        return $this->about;
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setSurname($surname)
    {
        $this->surname = $surname;
    }
    public function setPhone($phone){
        $this->phone = $phone;
    }
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function setSocial($social)
    {
        $this->social = $social;
    }

    public function setAbout($about)
    {
        $this->about = $about;
    }

    public function setAge($age)
    {
        $this->age = $age;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }
    abstract public function toString();
}