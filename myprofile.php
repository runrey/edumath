<?php
include "connect.php";
require_once('models/People.php');
require_once('models/Users.php');
session_start();

//if user not logged in, this page will not be showed
if(!isset($_SESSION['id'])) header("Location: login.php");

$stmt = $link->prepare("SELECT * FROM users WHERE user_id = ? LIMIT 1");
$stmt->bind_param("i", $_SESSION['id']);

$stmt->execute();

$result = $stmt->get_result();

$row = $result->fetch_assoc();
if ($row == null) return null;
$user = new Users($row['email'], $row['name'], $row['surname'], $row['phone'], $row['password'], $row['social_link'], $row['about'], $row['age'], $row['city']);

if ($user == null) {
    header("Location: index.php?logout");
    return;
}

if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['name']);
    unset($_SESSION['id']);
    header("location: index.php");
}

if (isset($_GET['add'])) {
    $stmt = $link->prepare("INSERT INTO users_courses_rel(user_id, courses_id) VALUES (?,?)");
    $stmt->bind_param("ii", $_SESSION['id'], $_GET['add']);
    $results = $stmt->execute();

    $stmt = $link->prepare("SELECT * FROM lesson_lines WHERE lesson_id in (SELECT lesson_id FROM lessons WHERE course_id = ?)");
    $stmt->bind_param("i", $_GET['add']);
    $stmt->execute();
    $res = $stmt->get_result();

    $stmt->close();

    $zero = 0;
    if (mysqli_num_rows($res) > 0) {
        while ($row = $res->fetch_assoc()) {
            $stmt1 = $link->prepare("INSERT INTO user_progress(user_id, course_id, lesson_line_id, achieved) VALUES (?,?,?,?)");
            $stmt1->bind_param("iiii", $_SESSION['id'], $_GET['add'], $row['lesson_line_id'], $zero);
            $results = $stmt1->execute();
        }
        $stmt1->close();
    }
}

if (isset($_GET['teacher'])) {
    $stmt = $link->prepare("UPDATE users SET type=2 WHERE user_id=?");
    $stmt->bind_param("ii", $_SESSION['id']);
    $results = $stmt->execute();
    $stmt->close();
    $_SESSION['type'] = 2;
}

if (isset($_GET['alert'])) {
    echo '<script type="text/JavaScript"> 
     alert("This course is already added!");
     </script>'
    ;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.2/font/bootstrap-icons.css">
    <title>eduMath | <?php echo $user->getName(); ?> <?php echo $user->getSurname(); ?></title>
    <link rel="stylesheet" href="css/index.css">
</head>
<body>

<!-- Navbar -->
<header>
    <div class="container-sm" >
        <nav class="navbar fixed-top navbar-expand-xl navbar-light bg-white" style="font-size: 1.2rem">
            <div class="container">
                <a class="navbar-brand text-secondary" style="font-size: 2.2rem;" href="index.php">eduMath</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav me-auto mb-2 mb-xl-0">
                        <li class="nav-item">
                            <a class="nav-link" aria-current="page" href="index.php#home">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php#about">About</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="index.php#contacts">Contacts</a>
                        </li>
                    </ul>
                    <ul class="navbar-nav mb-2 mb-xl-0">
                        <?php
                        if (!isset($_SESSION['id'])) {
                            echo "<li class='nav-item' ><a class='btn btn-secondary' href='login.php'>Login</a></li>";
                        }
                        else{
                            if ($_SESSION['type'] == 2) {
                                echo
                                "<li class='nav-item'><a class='nav-link lang' href='constructor.php' key='constructor'>Constructor</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                            }
                            else{
                                echo
                                "<li class='nav-item'><a class='nav-link lang' href='courses.php' key='courses'>Courses</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='myprofile.php' key='myprofile'>My profile</a></li>
                             <li class='nav-item'><a class='nav-link lang' href='index.php?logout' key='logout'>Logout</a></li>";
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</header>

<!--User Information -->
<div class="container mt-5">
    <div class="row d-flex justify-content-center" >
        <div class="col-md-10 mt-5 pt-5">
            <div class="row shadow bg-dark">
                <div class="col-sm-4 rounded-left align-self-center">
                    <div class="card-block text-center text-white">
                        <i class="bi bi-person mt-5" style="font-size: 8rem;"></i>
                        <h2 class="font-weight-bold"><?php echo $user->getName(); ?> <?php echo $user->getSurname(); ?></h2>
                        <p class="text-white"><?php echo $user->toString(); ?></p>
                        <a href="change_profile.php" title="Update profile information"><i class="bi bi-pencil-square  mt-5" style="font-size: 2rem;" style="font-size: 1.2rem;"></i></a>
                    </div>
                </div>
                <div class="col-sm-8 bg-light rounded-right">
                    <h3 class="mt-3  mb-4 text-center">Profile</h3>
                    <div class="row">
                        <div class="col-sm-6">
                            <p class="font-weight-bold text-secondary">Email: </p>
                            <h6 class="text-muted"><?php echo $user->getEmail(); ?></h6>
                        </div>
                        <div class="col-sm-6">
                            <p class="font-weight-bold text-secondary">Phone: </p>
                            <h6 class="text-muted"><?php echo $user->getPhone(); ?></h6>
                        </div>
                    </div>
                    <h5 class="mt-3 text-secondary">Additional information</h5>
                    <hr class="badge-secondary border-3">
                    <div class="row">
                        <div class="col-sm-4">
                            <h6 class="font-weight-bold text-secondary">Age: <?php echo $user->getAge(); ?></h6>
                        </div>
                        <div class="col-sm-4">
                            <h6 class="font-weight-bold text-secondary">City: <?php echo $user->getCity(); ?> </h6>
                        </div>
                        <div class="col-sm-4">
                            <a class="font-weight-bold text-secondary" href="<?php echo $user->getSocial(); ?>">Social link: <?php echo $user->getSocial(); ?></a>
                        </div>
                    </div>
                    <h5 class="mt-3 text-secondary">About</h5>
                    <hr class="badge-secondary border-3">
                    <div class="row">
                        <div class="col">
                            <p class="font-weight-bold text-secondary"><?php echo $user->getAbout(); ?> </p>
                        </div>
                    </div>

                    <?php
                    if ($_SESSION['type'] != 2) {
                        echo "<hr class='badge-secondary border-3'>
                                <a href='myprofile.php?teacher' class='btn btn-success lang mb-2'>Teacher account</a>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container" style="margin-top: 100px;">
    <div class="row justify-content-center" style="text-align: center;">
        <div class="col-4">
            <h1>My courses</h1>
            <hr class="badge-secondary mt-0 w-50 mx-auto">
        </div>
    </div>
</div>

<!-- Courses section -->
<div class="courses">
    <div class="container">
        <div class="row mt-5 justify-content-center">
            <?php
            $stmt = $link->prepare("SELECT * FROM courses WHERE course_id in (SELECT courses_id FROM users_courses_rel WHERE user_id = ?)");
            $stmt->bind_param("i", $_SESSION['id']);
            $stmt->execute();
            $res = $stmt->get_result();

            $stmt->close();

            if (mysqli_num_rows($res) > 0) {
                while ($row = $res->fetch_assoc()) {
                    echo '
            <div class="col-lg-2 col-sm-6 mb-4">
                <div class="card">
                    <img src="images/course_example_image.png" class="card-img-top" alt="card-grid-image">
                    <div class="card-body">
                        <h5 class="card-title" style="overflow:hidden;height: 50px;">'.$row["name"].'</h5>
                        <p class="card-text" style="overflow:hidden;height: 70px;"><small class="text-muted">'.$row["description"].'</small></p>
                        <a href="course_content.php?id='.$row["course_id"].'" class="btn btn-outline-dark mt-2 ">Continue course</a>
                    </div>
                </div>
            </div>';}}
            else{
                echo'<h1>No course found!</h1>';
            }
            ?>
        </div>
    </div>
</div>


<!-- Including custom js file and bootstrap js files -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script src="https://use.fontawesome.com/99e1cb3fcf.js"></script>
</body>
</html>	