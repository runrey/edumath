<?php
include "connect.php";
require_once "models/People.php";
include "models/Users.php";

$error = array();
if(!empty($_POST['email']) && !empty($_POST['name']) && !empty($_POST['surname']) && !empty($_POST['phone']) && !empty($_POST['password'])){
    $email = $_POST['email'];
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $phone = $_POST['phone'];
    $password = md5($_POST['password']);
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
        array_push($error, "Email is not correct!");
    }
    if(count($error) == 0){
        $stmt = $link->prepare("INSERT INTO users(email, name, surname, phone, password, type) VALUES (?,?,?,?,?,1)");
        $stmt->bind_param("sssss", $email, $name, $surname, $phone, $password);
        $results = $stmt->execute();
        array_push($error, "success");
        $stmt->close();

    }
}

else{
    array_push($error, "Not all fields are filled!");
}

echo json_encode($error);
?>