<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>eduMath | Sign up</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/index.css">
</head>
<body>
<div class="login_main">
    <form>
        <div class="login_form_body">
            <h2>Welcome back!</h2>
            <div class="login_form_elems">
                <div class="reg-status">
                    <p id="reg-status"></p>
                </div>
                <div class="login_form_elem">
                    <label for="email">E-MAIL</label>
                    <input type="email" id="email" name="email" required>
                </div>
                <div class="login_form_elem">
                    <label for="name">Name</label>
                    <input type="text" id="name" name="name" required>
                </div>
                <div class="login_form_elem">
                    <label for="surname">Surname</label>
                    <input type="text" id="surname"  name="surname" required>
                </div>
                <div class="login_form_elem">
                    <label for="phone">Phone</label>
                    <input type="text" id="phone" name="phone" onkeyup="return validatePhone(this.value);" pattern="[0-9]{10}" required>
                    <small>Example: 87775784707</small>
                </div>
                <div class="login_form_elem">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" minlength="8" pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*_=+-]).{8,16}$" required>
                    <small>
                        <ul>
                            <li>- At least one lowercase letter(a - z)</li>
                            <li>- At least one uppercase letter(A - Z)</li>
                            <li>- At least one numeric value(0-9)</li>
                            <li>- At least one special symbol(!@#$%^&*=+-_)</li>
                            <li>- The total length should be greater than or equal to 8 and less or equal to 16</li>
                        </ul>
                    </small>
                </div>

                <div class="login_form_elem">
                    <button class="btn btn-warning submit" name="submit" type="button">Submit</button>
                    <p>Already have an account? <a href="login.php">Login here</a></p>
                </div>
            </div>
        </div>
        <!--        <div class="login_form_body">-->
        <!--            <div class="login_form_elem">-->
        <!--                <label for="code">Enter code form mail which send to your e-amil:</label>-->
        <!--                <input type="text" id="code" name="code" required>-->
        <!--                <button class="btn btn-warning submit" name="submit" type="submit">Submit</button>-->
        <!--            </div>-->
        <!--        </div>-->
    </form>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script>
    var availableEmail = false;
    var properPhone = false;
    $(document).ready(function() {
        $(".submit").click(function(){
            event.preventDefault();
            var email = $('#email').val();
            var name = $('#name').val();
            var surname = $('#surname').val();
            var phone = $('#phone').val();
            var password = $('#password').val();
            $.post('registration.php', { email : email, name : name, surname : surname, phone : phone, password : password },
                function(data, status){
                    $('.reg-status').fadeIn(300);

                    var dat = JSON.parse(data);
                    if(dat[0]!=="success"){
                        var str = "";
                        for(var i = 0; i<dat.length; i++){
                            str += dat[i] + "<br>";
                        }
                        $('#reg-status').html(str).css('color', 'red');
                    }
                    else{
                        window.location.href = "login.php";
                    }
                });
        });
    });

    $("#email").bind('input', function () {
        var email = $('#email').val();
        if(email !== '') {
            $.ajax({
                type: "POST",
                url: "checker.php",
                data: { email : email },
                accepts: 'application/json; charset=utf-8',
                success: function (data) {
                    data = JSON.parse(data);
                    for (var key in data)
                    {
                        if(data.message === 'available'){
                            $('.reg-status').fadeIn(300);
                            $('#reg-status').text("E-mail is available").css('color', 'green');
                            availableEmail = true
                        }
                        if(data.message === 'not-available'){
                            $('.reg-status').fadeIn(500);
                            $('#reg-status').text("E-mail is not available").css('color', 'red');
                            availableEmail = false
                        }
                    }
                },
                error: function (httpRequest, status, error) {
                    $('#reg-status').text('Error: ' + error + ', ' + httpRequest.status).css('color', 'red');
                }
            });
        }
        if(email.length === 0){
            $('.reg-status').fadeOut(300);
        }
    });

    function validatePhone(phone){

        if (phone === '' || !phone.match(/^8[0-9]{10}$/)){
            $('#phone').css({'border': '2px solid red'});
            properPhone = false;
            return false;
        }
        else{
            $('#phone').css({'border': '2px solid green'});
            properPhone = true; d
            return true;
        }
    }
</script>
<script src="js/index.js"></script>
</body>
</html>