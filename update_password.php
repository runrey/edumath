<?php
include "connect.php";
require_once "models/People.php";
include "models/Users.php";
session_start();
if(isset($_POST['save_pass']))
{
    $newpass1 = $_POST['newpass1'];

    $stmt = $link->prepare("SELECT * FROM users WHERE user_id=?");
    $stmt->bind_param("i", $_SESSION['id']);
    $stmt->execute();
    $res = $stmt->get_result();

    $stmt->close();

    if(mysqli_num_rows($res)>0){
        $row = $res->fetch_assoc();
        $md5pass = md5($newpass1);
        $stmt = $link->prepare("UPDATE users SET password=? WHERE user_id=?");
        $stmt->bind_param("si",  $md5pass, $_SESSION['id']);
        $results = $stmt->execute();
        $stmt->close();
        $_SESSION['status'] = "Password updated successfully";
        }
    header("Location: admin_users.php");
}
?>