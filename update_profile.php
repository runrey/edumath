<?php
include "connect.php";
require_once "models/People.php";
include "models/Users.php";
session_start();
if(isset($_POST['update_pass']))
{
    $curpass = $_POST['curpass'];
    $newpass1 = $_POST['newpass1'];
    $newpass2 = $_POST['newpass2'];

    $stmt = $link->prepare("SELECT * FROM users WHERE user_id=?");
    $stmt->bind_param("i", $_SESSION['id']);
    $stmt->execute();
    $res = $stmt->get_result();

    $stmt->close();

    if(mysqli_num_rows($res)>0){
        $row = $res->fetch_assoc();
        if($curpass) {
            if (md5($curpass) == $row['password'])
            {
                if ($newpass1 == $newpass2)
                {
                    $md5pass = md5($newpass1);
                    $stmt = $link->prepare("UPDATE users SET password=? WHERE user_id=?");
                    $stmt->bind_param("si",  $md5pass, $_SESSION['id']);
                    $results = $stmt->execute();
                    $stmt->close();
                    $_SESSION['status'] = "Your password updated successfully";
                }
                else
                {
                    $_SESSION['status'] = "Your new and retype password is not match";
                }
            }
            else
            {
                $_SESSION['status'] = "Your old password is wrong";
            }
        }
    }
    header("Location: change_password.php");
}


if(isset($_POST['update_email']))
{
    $email = $_POST['email'];



    $stmt = $link->prepare("UPDATE users SET email=? WHERE user_id=?");
    $stmt->bind_param("si", $email, $_SESSION['id']);
    $results = $stmt->execute();
    $stmt->close();

    if($results) {
        $_SESSION['status'] = "Your email updated successfully";
    }else{
        $_SESSION['status'] = "Your email not updated successfully";
    }


    header("Location: change_email.php");
}

if(isset($_POST['update']))
{
    $name = $_POST['name'];
    $surname = $_POST['surname'];
    $phone = $_POST['phone'];


    $stmt = $link->prepare("UPDATE users SET name=?, surname=?, phone=? WHERE user_id=?");
    $stmt->bind_param("sssi", $name,$surname,$phone, $_SESSION['id']);
    $results = $stmt->execute();
    $stmt->close();

    if($results) {
        $_SESSION['status'] = "Your data updated successfully";
    }else{
        $_SESSION['status'] = "Your data not updated successfully";
    }


    header("Location: change_profile.php");
}

if(isset($_POST['update_additional_information']))
{
    $city = $_POST['city'];
    $age = $_POST['age'];
    $social = $_POST['social'];
    $about = $_POST['about'];


    $stmt = $link->prepare("UPDATE users SET city=?, age=?, social_link=?, about=? WHERE user_id=?");
    $stmt->bind_param("ssssi", $city,$age, $social, $about, $_SESSION['id']);
    $results = $stmt->execute();
    $stmt->close();

    if($results) {
        $_SESSION['status'] = "Your data updated successfully";
    }else{
        $_SESSION['status'] = "Your data not updated successfully";
    }


    header("Location: change_add_info.php");
}



?>